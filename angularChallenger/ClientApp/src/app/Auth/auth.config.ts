interface AuthConfig {
    CLIENT_ID: string;
    CLIENT_DOMAIN: string;
    AUDIENCE: string;
    REDIRECT: string;
    SCOPE: string;
};


export const AUTH_CONFIG: AuthConfig = {
    CLIENT_ID: 'aasp_vacinacao',
    CLIENT_DOMAIN: "sso.homolog.aasp.org.br/account/login",
    AUDIENCE: 'http://localhost:44303',
    REDIRECT: 'http://localhost:44303/associado/solicitacao',
    SCOPE: 'openid profile'
} 
