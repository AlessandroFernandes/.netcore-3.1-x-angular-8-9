import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { MaterialModule } from "src/assets/styles/Material.Module";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GeralComponent } from "./geral/geral.component";

@NgModule({
    declarations: [
        LoginComponent,
        GeralComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        MaterialModule,
        FormsModule,
        RouterModule,
        FontAwesomeModule
    ]
})
export class AdministrativoModule {}