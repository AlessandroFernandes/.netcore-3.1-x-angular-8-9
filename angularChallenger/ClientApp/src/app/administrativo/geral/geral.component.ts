import { Component } from "@angular/core";

import { faFileExcel } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'aasp-geral',
    templateUrl: './geral.component.html',
    styleUrls: [ './geral.component.css']
})
export class GeralComponent {

  faFile = faFileExcel;
}
