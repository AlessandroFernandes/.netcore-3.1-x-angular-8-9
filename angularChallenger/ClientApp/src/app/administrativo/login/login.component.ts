import { Component } from "@angular/core";

import { faUser, faKey } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'aasp-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    faUser = faUser;
    faKey = faKey;

}