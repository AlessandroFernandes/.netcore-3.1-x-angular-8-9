import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { FetchDataComponent } from "./associado/fetch-data/fetch-data.component";
import { SolicitacaoComponent } from "./associado/solicitacao/solicitacao.component";

import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from "./administrativo/login/login.component";
import { GeralComponent } from "./administrativo/geral/geral.component";

const router: Routes =  [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'fetch-data', component: FetchDataComponent },
  { path: 'associado/solicitacao',
    component: SolicitacaoComponent,
    //canActivate: [ AuthGuard]
  },
  { path: "a4dmi2n/login", component: LoginComponent},
  { path: "a4dmi2n/geral", component: GeralComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(router)],
  providers: [ AuthGuard ]
})

export class AppRouteModule {}
