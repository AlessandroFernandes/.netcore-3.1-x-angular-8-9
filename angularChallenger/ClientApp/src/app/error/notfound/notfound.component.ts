import { Component } from "@angular/core";

@Component({
    selector: 'aasp-notfound',
    templateUrl: './notfound.component.html',
    styleUrls: ['./notfound.component.css']
})
export class NotFoundComponent {}