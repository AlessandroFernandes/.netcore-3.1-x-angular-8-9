import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";


import { MaterialModule } from "src/assets/styles/Material.Module";
import { FetchDataComponent } from "./fetch-data/fetch-data.component";
import { SolicitacaoComponent } from './solicitacao/solicitacao.component';
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    FetchDataComponent,
    SolicitacaoComponent,
    NavMenuComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    RouterModule,
    FormsModule
   ]
})
export class AssociadoModule {}
