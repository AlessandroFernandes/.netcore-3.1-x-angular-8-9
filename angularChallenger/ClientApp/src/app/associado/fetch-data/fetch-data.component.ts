import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public forecasts: WeatherForecast[];

  data = [ {"Data": '05/03/2020', "Dose": 3, "valor" : "R$ 108,00", "Status": "Pago", "Atendimento": "Concluído"},
  {"Data": '05/03/2020', "Dose": 3, "valor" : "R$ 108,00", "Status": "Pago", "Atendimento": "Concluído"},
  {"Data": '05/03/2020', "Dose": 1, "valor" : "R$ 36,00", "Status": "Pago", "Atendimento": "Concluído"},
  {"Data": '05/03/2020', "Dose": 3, "valor" : "R$ 108,00", "Status": "Pago", "Atendimento": "Concluído"},
]

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));
  }
}

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}


