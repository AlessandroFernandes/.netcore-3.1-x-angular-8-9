import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

import { Dependente } from './dependente/dependente';

@Component({
  selector: 'aasp-solicitacao',
  templateUrl: './solicitacao.component.html',
  styleUrls: ['./solicitacao.component.css']
})
export class SolicitacaoComponent implements OnInit {

  @Input() dependente = { nome: '', nasc: ''};
  pessoas = [];
  valorTotal = 0;
  valorDose = 36;

  constructor( private auth: AuthService) { }

  associado = {};


  ngOnInit() {
    this.BuscarAssociado();
    console.log(this.pessoas);
  }

  EnviarFormulario() {
    console.log(this.pessoas);
  }

  BuscarAssociado(): void {
    this.associado = {nome: 'Alessandro Roberto da Cunha Fernandes', nasc: '1988-07-22', ativo: true};
    this.valorTotal =  1 * this.valorDose;
  }

  DeletarAssociado() {
    this.associado = { ativo: false };
    // tslint:disable-next-line:no-unused-expression
    this.valorTotal - this.valorDose;
  }

  AdicionarDependente(): void {
    if (this.dependente.nome === '' || this.dependente.nasc === '') {
      return null;
    }
    this.pessoas.push({ nome: this.dependente.nome, nasc: this.dependente.nasc });
    this.valorTotal = this.pessoas.length * this.valorDose;
    this.dependente.nome = '';
    this.dependente.nasc = '';
  }


}
