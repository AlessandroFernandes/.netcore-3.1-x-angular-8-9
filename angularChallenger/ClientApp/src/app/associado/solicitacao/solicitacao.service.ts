import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({ providedIn: 'root'})
export class SolicitacaoService {

  constructor(private http: HttpClient){}

  AdicionarDependente(dados): void {
    this.http.post(`http://localhost:3000/posts`, dados);
  }
}
