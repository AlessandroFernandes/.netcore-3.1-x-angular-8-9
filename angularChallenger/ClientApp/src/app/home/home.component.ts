import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'aasp-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
 
  constructor(public authService: AuthService){}


  login = () => this.authService.login()
}

