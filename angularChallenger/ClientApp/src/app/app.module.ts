import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRouteModule } from './app.routing';

import { AppComponent } from './app.component';
import { AssociadoModule } from './associado/associado.module';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from './auth/auth.service';

import {OAuthModule} from 'angular-oauth2-oidc';
import { AdministrativoModule } from './administrativo/administrativo.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    AppRouteModule,
    AssociadoModule,
    AdministrativoModule,
    BrowserAnimationsModule,
    OAuthModule.forRoot()
  ],
  providers: [ AuthService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
